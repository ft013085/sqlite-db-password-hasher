import sqlite3
import hashlib

def create_table(conn):
    conn.execute('''CREATE TABLE IF NOT EXISTS USERS
                    (username TEXT, password TEXT)''')

# Fetch Operations
def get_users(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM USERS")
    users = cur.fetchall()
    cur.close()
    return users

def print_table(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM USERS")
    users = cur.fetchall()
    for eachUser in users:
        print(f"Username: {eachUser[0]}, Hashed Password: {eachUser[1]}")
    cur.close()

# Verification Function
def verify_decision(message):
    while True:
        decision = input(f"{message} (yes/no): ")
        if decision.lower() == "yes":
            return True
        elif decision.lower() == "no":
            return False
        else:
            print("Invalid input. Please enter 'yes' or 'no'.")

# Manipulation Functions
def insert_user(conn, username, password):
    if verify_decision(f"Do you want to insert user '{username}'?"):
        conn.execute("INSERT INTO USERS (username, password) VALUES (?, ?)", (username, password))
        conn.commit()
        print(f"User '{username}' inserted.")
    else:
        print("User not inserted.")

def update_username(conn, current_username, new_username):
    if verify_decision(f"Do you want to change username '{current_username}' to '{new_username}'?"):
        conn.execute("UPDATE USERS SET username = ? WHERE username = ?", (new_username, current_username))
        conn.commit()
        print(f"Username '{current_username}' changed to '{new_username}'.")
    else:
        print("Username not changed.")

def hash_password(password):
    hash_obj = hashlib.sha256()  # Create a hash object
    hash_obj.update(password.encode('utf-8'))
    return hash_obj.hexdigest()

def hash_all_passwords(conn, users):
    if verify_decision("Do you want to hash all user passwords?"):
        cur = conn.cursor()
        for eachUser in users:
            username = eachUser[0]
            password = eachUser[1]
            hashed_password = hash_password(password)
            cur.execute("UPDATE USERS SET password = ? WHERE username = ?", (hashed_password, username))
        conn.commit()
        cur.close()
        print("User passwords hashed.")
    else:
        print("User passwords not hashed.")

def update_password(conn, username, new_password):
    if verify_decision(f"Do you want to change password for user '{username}'?"):
        conn.execute("UPDATE USERS SET password = ? WHERE username = ?", (new_password, username))
        conn.commit()
        print(f"Password for user '{username}' changed.")
    else:
        print("Password not changed.")

def update_user(conn, current_username, new_username, new_password):
    if verify_decision(f"Do you want to update user '{current_username}' to '{new_username}'?"):
        hashed_password = hash_password(new_password)
        cur = conn.cursor()
        cur.execute("UPDATE USERS SET username = ?, password = ? WHERE username = ?", (new_username, hashed_password, current_username))
        conn.commit()
        cur.close()
        print(f"User '{current_username}' updated to '{new_username}'.")
    else:
        print("User not updated.")

def clear_table(conn):
    if verify_decision("Are you sure you want to clear the table?"):
        conn.execute("DELETE FROM USERS")
        conn.commit()
        print("Table cleared.")
    else:
        print("Table not cleared.")

def main():
    # Connect to the SQLite database
    conn = sqlite3.connect('test.db')

    # Create USERS table if it doesn't exist
    create_table(conn)

    # Insert a new user 'bob' with password 'Fishy123'
    insert_user(conn, 'bob', 'Fishy123')

    # Retrieve all users and hash their passwords
    users = get_users(conn)
    hash_all_passwords(conn, users)

    # Print the table after hashing passwords
    print("Table after updating passwords:")
    print_table(conn)

    # Update username 'bob' to 'alice' and change the password to 'NewPassword456'
    update_user(conn, 'bob', 'alice', 'NewPassword456')

    # Print the table after updating username and password
    print("Table after changing username and password:")
    print_table(conn)

    # Clear the USERS table
    clear_table(conn)

    # Close the database connection
    conn.close()

if __name__ == "__main__":
    main()
